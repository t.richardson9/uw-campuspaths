/*
 * Copyright (C) 2023 Hal Perkins.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Winter Quarter 2023 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

package campuspaths;

import campuspaths.utils.CORSFilter;
import com.google.gson.Gson;
import pathfinder.CampusMap;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

import java.util.ArrayList;
import java.util.List;

/**Spark server that runs the CampusMap GUI's backend computations
 *
 */
public class SparkServer {
    /**Main program that runs server.
     *
     * @param args command line args
     */
    public static void main(String[] args) {
        CORSFilter corsFilter = new CORSFilter();
        corsFilter.apply();
        //declares campusmap to load data into map interface and initialize the campus map
        Gson gson = new Gson();
        CampusMap campusMap = new CampusMap();
        // The above two lines help set up some settings that allow the
        // React application to make requests to the Spark server, even though it
        // comes from a different server.
        // You should leave these two lines at the very beginning of main().

        //getNames gets the building names and sends them to the app as a json file
        Spark.get("/getNames", new Route() {
            @Override
            public Object handle(Request request, Response response) throws Exception {
                //captures key set and values of names map and formats string to send
                //short name followed by long name seperateds by hyphen for easy parsing
                List<String> shortNameList = new ArrayList<>(campusMap.buildingNames().keySet());
                List<String> longNameList = new ArrayList<>((campusMap.buildingNames().values()));
                List<String> totalList = new ArrayList<>();
                for(int i = 0; i<shortNameList.size(); i++){
                    totalList.add(shortNameList.get(i)+"-"+longNameList.get(i));
                }
                //returns formatted string of names as a json file
                return gson.toJson(totalList);
            }
            });

        //route to find and return shortest path to GUI as json
        Spark.get("/findPath", new Route() {
            @Override
            public Object handle(Request request, Response response) throws Exception {
                String startString = request.queryParams("start");
                String endString = request.queryParams("end");
                //stops program if start or stop is null
                if(startString == null || endString == null) {
                    Spark.halt(400, "must have start and end");
                }
                //returns shortest path between points from Dijkstra algorithm as a json file
                return gson.toJson(campusMap.findShortestPath(startString,endString));
            }

        });
    }

}
