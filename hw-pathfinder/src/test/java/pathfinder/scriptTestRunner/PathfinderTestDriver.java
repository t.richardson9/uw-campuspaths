/*
 * Copyright (C) 2023 Hal Perkins.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Winter Quarter 2023 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

package pathfinder.scriptTestRunner;

import graph.Edge;
import graph.GraphMap;
import graph.Node;
import pathfinder.datastructures.PathFinder;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * This class implements a test driver that uses a script file format
 * to test an implementation of Dijkstra's algorithm on a graph.
 */
public class PathfinderTestDriver {

    PathFinder<String> pathFinder = new PathFinder<>();
    private final Map<String,GraphMap<String,Double>> graphs = new HashMap<>();
    private final PrintWriter output;
    private final BufferedReader input;
    // Leave this constructor public
    public PathfinderTestDriver(Reader r, Writer w) {
        output = new PrintWriter(w);
        input = new BufferedReader(r);
        // See GraphTestDriver as an example.
    }

    /**
     * @throws IOException if the input or output sources encounter an IOException
     * @spec.effects Executes the commands read from the input and writes results to the output
     **/
    // Leave this method public
    public void runTests() throws IOException {
        String inputLine;
        while((inputLine = input.readLine()) != null) {
            if((inputLine.trim().length() == 0) ||
                    (inputLine.charAt(0) == '#')) {
                // echo blank and comment lines
                output.println(inputLine);
            } else {
                // separate the input line on white space
                StringTokenizer st = new StringTokenizer(inputLine);
                if(st.hasMoreTokens()) {
                    String command = st.nextToken();

                    List<String> arguments = new ArrayList<>();
                    while(st.hasMoreTokens()) {
                        arguments.add(st.nextToken());
                    }

                    executeCommand(command, arguments);
                }
            }
            output.flush();
        }
    }

    private void executeCommand(String command, List<String> arguments) {
        try {
            switch(command) {
                case "CreateGraph":
                    createGraph(arguments);
                    break;
                case "AddNode":
                    addNode(arguments);
                    break;
                case "AddEdge":
                    addEdge(arguments);
                    break;
                case "ListNodes":
                    listNodes(arguments);
                    break;
                case "ListChildren":
                    listChildren(arguments);
                    break;
                case "FindPath":
                    findPath(arguments);
                    break;
                default:
                    output.println("Unrecognized command: " + command);
                    break;
            }
        } catch(Exception e) {
            String formattedCommand = command;
            formattedCommand += arguments.stream().reduce("", (a, b) -> a + " " + b);
            output.println("Exception while running command: " + formattedCommand);
            e.printStackTrace(output);
        }
    }

    private void createGraph(List<String> arguments) {
        if(arguments.size() != 1) {
            throw new CommandException("Bad arguments to CreateGraph: " + arguments);
        }

        String graphName = arguments.get(0);
        createGraph(graphName);
    }

    private void createGraph(String graphName) {
        GraphMap<String,Double> g1 = new GraphMap<>(graphName);
        graphs.put(graphName, g1);
        output.println("created graph "+ graphName);
    }

    private void addNode(List<String> arguments) {
        if(arguments.size() != 2) {
            throw new CommandException("Bad arguments to AddNode: " + arguments);
        }

        String graphName = arguments.get(0);
        String nodeName = arguments.get(1);

        addNode(graphName, nodeName);
    }

    private void addNode(String graphName, String nodeName) {
        //create new graph
        GraphMap<String,Double> g1 = graphs.get(graphName);
        //create new node
        Node<String,Double> n = new Node<>(nodeName);
        //add new node to the list
        g1.addNode(n);
        output.println("added node " + n.getName() +" to " +graphName);
    }

    private void addEdge(List<String> arguments) {
        if(arguments.size() != 4) {
            throw new CommandException("Bad arguments to AddEdge: " + arguments);
        }

        String graphName = arguments.get(0);
        String parentName = arguments.get(1);
        String childName = arguments.get(2);
        String edgeLabel = arguments.get(3);
        Double edge = Double.parseDouble(arguments.get(3));

        addEdge(graphName, parentName, childName, edgeLabel, edge);
    }

    private void addEdge(String graphName, String parentName, String childName,
                         String edgeLabel, Double edge) {
        GraphMap<String,Double> g1 = graphs.get(graphName);
        Node<String,Double> parent= g1.getSpecificNode(parentName);
        Node<String,Double> child = g1.getSpecificNode(childName);
        //checks to make sure edge is not in the graph already
        if(g1.getSpecificNode(parentName).getChildrenNodes().contains(g1.getSpecificNode(childName))){
            if(g1.getSpecificNode(parentName).GetNodeMap().get(g1.getSpecificNode(childName)).contains(g1.getSpecificEdge(edgeLabel)));
            output.println("Edge already in graph");
        }else {
            //adds edge to the graph
            g1.addEdge(edge, parent, child);
            output.println(String.format("added edge %.3f",edge) + " from " + parentName + " to " + childName + " in " + graphName);
        }
    }

    private void listNodes(List<String> arguments) {
        if(arguments.size() != 1) {
            throw new CommandException("Bad arguments to ListNodes: " + arguments);
        }

        String graphName = arguments.get(0);
        listNodes(graphName);
    }

    private void listNodes(String graphName) {
        GraphMap<String,Double> g = graphs.get(graphName);
        List<Node<String,Double>> nodeList = new ArrayList<>();
        //sorts list using implemented string comparator
        List<Node<String,Double>> nodes = g.getNodes();
        nodeList.addAll(nodes);
        nodeList.sort(new Comparator<Node<String,Double>>() {
            @Override
            public int compare(Node<String,Double>o1, Node<String,Double> o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1.toString(), o2.toString());
            }
        });
        //if list is empty outputs empty string, else returns formatted output
        if(nodes.size()!=0) {
            output.println(graphName + " contains: " + nodeList.toString().replace("[", "").replace("]", "").replace(",", ""));
        }else{
            output.println(graphName + " contains:");
        }
    }

    private void listChildren(List<String> arguments) {
        if(arguments.size() != 2) {
            throw new CommandException("Bad arguments to ListChildren: " + arguments);
        }

        String graphName = arguments.get(0);
        String parentName = arguments.get(1);
        listChildren(graphName, parentName);
    }

    private void listChildren(String graphName, String parentName) {
        GraphMap<String,Double> g = graphs.get(graphName);
        List<String> newChildren = new ArrayList<>();
        //iterates through the list and returns node with name equal to parentName
        Node<String,Double> tempNode = g.getSpecificNode(parentName);
        HashMap<Node<String,Double>,List<Edge<String,Double>>> temp = tempNode.GetNodeMap();
        //gets the edges from each child node
        for(Map.Entry<Node<String,Double>,List<Edge<String,Double>>> tempMap: temp.entrySet()) {
            List<Edge<String,Double>> edges = tempMap.getValue();
            //sets string value to child = edge for all edges
            for(int i = 0; i < edges.size();i++) {
                newChildren.add(tempMap.getKey() + "=" + edges.get(i));
            }
        }

        newChildren.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
            }
        });
        output.println("the children of " + parentName +  " in " + graphName+ " are: " + newChildren.toString().replace("[","").replace("]","").replace(",",")").replace("{","").replace("}","").replace("=","(") + ")");
    }
    private void findPath(List<String> arguments) {
        if(arguments.size() != 3) {
            throw new CommandException("Bad arguments to FindPath: " + arguments);
        }
        String graphName = arguments.get(0);
        String parentNode = arguments.get(1);
        String childNode = arguments.get(2);
        findPath(graphName,parentNode,childNode);
    }

    private void findPath(String graphName, String parent, String child){
        GraphMap<String,Double> g = graphs.get(graphName);
        List<Edge<String,Double>> path = pathFinder.findPath(g,parent,child);
        // if there is not a node in the graph
        if(g.getSpecificNode(parent) == null || g.getSpecificNode(child)== null){
            //
            if(g.getSpecificNode(parent)==null){
                output.println("unknown: " +parent);
            }
            //if the child node is null or not in the graph
            if(g.getSpecificNode(child)==null){
                output.println("unknown: "+child);
            }
            return;
        }
        output.println("path from " + parent + " to " + child + ":");
        if(path.size() == 0) {
            output.println("no path found");
            return;
        }
        Double cost = (double)0;
        for (int i = 0; i < path.size() ; i++) {
            output.println(path.get(i).getParent() + " to " + path.get(i).getChild() + String.format(" with weight %.3f",path.get(i).getLabel()));
            cost+=path.get(i).getLabel();
        }
        output.println(String.format("total cost: %.3f",cost));
    }

    /**
     * This exception results when the input file cannot be parsed properly
     **/
    static class CommandException extends RuntimeException {

        public CommandException() {
            super();
        }

        public CommandException(String s) {
            super(s);
        }

        public static final long serialVersionUID = 3495;
    }
}
