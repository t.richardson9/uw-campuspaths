/*
 * Copyright (C) 2023 Hal Perkins.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Winter Quarter 2023 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

package pathfinder;

import graph.Edge;
import graph.Node;
import pathfinder.datastructures.Path;
import pathfinder.datastructures.PathFinder;
import pathfinder.parser.CampusBuilding;
import pathfinder.parser.CampusPath;
import pathfinder.parser.CampusPathsParser;
import graph.GraphMap;
import pathfinder.datastructures.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

/**Class implements the modelAPI that connects the backend computations with the user interface
 *
 */
public class CampusMap implements ModelAPI {
//Abstraction Function:
//CampusMap represents a graph of vertices(points) and edges(paths). Each path starts
//at a point and ends at another point. Each node of the graph is contained
//in graph.nodes(each node is unique) and each edge is contained in graph.edges
//If there are no nodes or edges then CampusMap represents an empty graph

//Rep Invariant:
// pathfinder != null && graphMap1 != null
// && parser != null && names != null &&
// points != null

    PathFinder<Point> pathfinder = new PathFinder<>();
    GraphMap<Point,Double> graphMap1 = buildGraph();
    HashMap<String,String> names;
    HashMap<String,Point> points;
    CampusPathsParser parser = new CampusPathsParser();
    @Override
    public boolean shortNameExists(String shortName) {
        checkRep();
        return names.containsKey(shortName);
    }

    @Override
    public String longNameForShort(String shortName) {

        checkRep();
        return names.get(shortName);
    }

    @Override
    public Map<String, String> buildingNames() {
        checkRep();
        return names;
    }

    @Override
    public Path<String> findShortestPath(String startShortName, String endShortName) {
        checkRep();
        List<Edge<Point,Double>> paths = pathfinder.findPath(graphMap1, points.get(startShortName), points.get(endShortName));
        Path<String> path = new Path<>(points.get(startShortName));
        //loops through all edges in the shortest path and uses edges to extend path to be returned to interface
        for(int i = 0; i < paths.size(); i++){
            path = path.extend(paths.get(i).getChild().getName(),paths.get(i).getLabel());
        }
        checkRep();
        return path;
    }

    //constructs graph to be used in the Campus Map application
   private GraphMap<Point,Double> buildGraph() {
        GraphMap<Point,Double> graphMap = new GraphMap<>("graphMap");
        names = new HashMap<>();
        points = new HashMap<>();
        List<CampusBuilding> building = parser.parseCampusBuildings("campus_buildings.csv");
        List<CampusPath> paths = parser.parseCampusPaths("campus_paths.csv");
        //adds all building names and points to a map to access point information for each building
        for(int i = 0; i< building.size(); i++){
            Point p = new Point(building.get(i).getX(),building.get(i).getY());
            points.put(building.get(i).getShortName(),p);
            names.put(building.get(i).getShortName(),building.get(i).getLongName());
        }
        //creates GraphMap and adds all nodes as points and an edge between each node given by the path parser
        for(int j = 0; j < paths.size(); j++){
            Point p1 = new Point(paths.get(j).getX1(),paths.get(j).getY1());
            Point p2 = new Point(paths.get(j).getX2(),paths.get(j).getY2());
            graphMap.addNode(new Node<>(p1));
            graphMap.addNode(new Node<>(p2));
            graphMap.addEdge(paths.get(j).getDistance(),graphMap.getSpecificNode(p1),graphMap.getSpecificNode(p2));
        }

        return graphMap;
    }

    private void checkRep(){
        assert(parser != null &&graphMap1 != null && points != null && names!= null && pathfinder != null);
    }
}
