package pathfinder.datastructures;
import graph.GraphMap;
import graph.Node;
import graph.Edge;

import java.util.*;

/**Class that finds shortest path from A to B in a graph
 *
 * @param <N> Type parameter of Node type objects
 */
public class PathFinder<N> {

//Class is not an abstract method and has no rep invariant because it contains a
// single graph path method only

    /**Returns a list of edges that represent the shortest path from A to B in a graph
     *
     * @param graphMap Graph to find the path in
     * @param start Starting node in the path to find
     * @param end end node in the path to find
     * @return List of edges representing the shortest path from A to B
     */
    public List<Edge<N,Double>> findPath(GraphMap<N,Double> graphMap, N start, N end){
        //declares a new priority queue that compares Lists of edges for total cost and prioritizes lowest cost path first
        PriorityQueue<List<Edge<N, Double>>> active = new PriorityQueue<List<Edge<N, Double>>>(new Comparator<List<Edge<N, Double>>>() {
            @Override
            public int compare(List<Edge<N, Double>> o1, List<Edge<N, Double>> o2) {
                //cost of first path
                Double cost1 = (double) 0;
                for(int i = 0; i< o1.size(); i++){
                     cost1 = cost1 + o1.get(i).getLabel();
                }
                //cost of second path
                Double cost2 = (double) 0;
                for(int i = 0; i< o2.size(); i++){
                    cost2 = cost2 + o2.get(i).getLabel();
                }
                return cost1.compareTo(cost2);
            }
        });
        //creates new list of nodes we have already been to
        List<Node<N,Double>> finished = new ArrayList<>();
        //retrieves end node to be checked against the current node we have
        Node<N,Double> goalNode = graphMap.getSpecificNode(end);
        //terminates method and returns an empty arraylist if there is no start or end node found
        if (graphMap.getSpecificNode(start) == null || graphMap.getSpecificNode(end)==null){
            return new ArrayList<>();
        }
        //creates a path to use for starting point
        List<Edge<N,Double>> startingEdge = new ArrayList<>();
        Edge<N,Double> firstEdge = new Edge<N,Double>((double) 0, graphMap.getSpecificNode(start), graphMap.getSpecificNode(start));
        startingEdge.add(firstEdge);
        active.add(startingEdge);
        //this loop with run as long as there is still a path in active
        while(!active.isEmpty()){
            //minPath is the current smallest path
            List<Edge<N,Double>> minPath = active.remove();
            //minDest is the last node in minPath
            Node<N,Double> minDest = minPath.get(minPath.size()-1).getChild();
            //if current node is the goal node minpath is returned and 0 path.segment is removed
            if(minDest.equals(goalNode)) {
                minPath.remove(0);
                return minPath;
            }
            if(!finished.contains(minDest)){
                //processes children nodes and updates paths to the children nodes, then stores paths in active
                List<Node<N,Double>> children = graphMap.getSpecificNode(minDest.getName()).getChildrenNodes();
                for(int i = 0; i < children.size(); i++){
                    if(!finished.contains(children.get(i))) {
                        //creates a new path to append childNode onto
                        List<Edge<N, Double>> newPath = new ArrayList<>(minPath);
                        newPath.add(minDest.getClosestEdge(children.get(i)));
                        //adds current child path onto the queue
                        active.add(newPath);
                    }
                }
                //adds current node to finished list
                finished.add(minDest);
            }
        }
        //if there is no path fround, returns an empty list
        return new ArrayList<>();
    }


}
