/*
 * Copyright (C) 2023 Hal Perkins.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Winter Quarter 2023 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

package marvel.scriptTestRunner;

import graph.Edge;
import graph.GraphMap;
import graph.Node;
import marvel.MarvelParser;
import marvel.MarvelPaths;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.*;

/**
 * This class implements a testing driver which reads test scripts from
 * files for testing Graph, the Marvel parser, and your BFS algorithm.
 */
public class MarvelTestDriver {
    // ***************************
    // ***  JUnit Test Driver  ***
    // ***************************

    /**
     * String -> Graph: maps the names of graphs to the actual graph
     **/
    private final Map<String, MarvelPaths> graphs = new HashMap<String, MarvelPaths>();
    private final PrintWriter output;
    private final BufferedReader input;
    MarvelParser parser = new MarvelParser();


    /**
     * @spec.requires r != null && w != null
     * @spec.effects Creates a new GraphTestDriver which reads command from
     * {@code r} and writes results to {@code w}
     **/
    // Leave this constructor public
    public MarvelTestDriver(Reader r, Writer w) {
        input = new BufferedReader(r);
        output = new PrintWriter(w);
    }

    /**
     * @throws IOException if the input or output sources encounter an IOException
     * @spec.effects Executes the commands read from the input and writes results to the output
     **/
    // Leave this method public
    public void runTests() throws IOException {
        String inputLine;
        while((inputLine = input.readLine()) != null) {
            if((inputLine.trim().length() == 0) ||
                    (inputLine.charAt(0) == '#')) {
                // echo blank and comment lines
                output.println(inputLine);
            } else {
                // separate the input line on white space
                StringTokenizer st = new StringTokenizer(inputLine);
                if(st.hasMoreTokens()) {
                    String command = st.nextToken();

                    List<String> arguments = new ArrayList<>();
                    while(st.hasMoreTokens()) {
                        arguments.add(st.nextToken());
                    }

                    executeCommand(command, arguments);
                }
            }
            output.flush();
        }
    }

    private void executeCommand(String command, List<String> arguments) {
        try {
            switch(command) {
                case "CreateGraph":
                    createGraph(arguments);
                    break;
                case "AddNode":
                    addNode(arguments);
                    break;
                case "AddEdge":
                    addEdge(arguments);
                    break;
                case "ListNodes":
                    listNodes(arguments);
                    break;
                case "ListChildren":
                    listChildren(arguments);
                    break;
                case "FindPath":
                    findPath(arguments);
                    break;
                case "LoadGraph":
                    loadGraph(arguments);
                    break;
                default:
                    output.println("Unrecognized command: " + command);
                    break;
            }
        } catch(Exception e) {
            String formattedCommand = command;
            formattedCommand += arguments.stream().reduce("", (a, b) -> a + " " + b);
            output.println("Exception while running command: " + formattedCommand);
            e.printStackTrace(output);
        }
    }

    private void createGraph(List<String> arguments) {
        if(arguments.size() != 1) {
            throw new CommandException("Bad arguments to CreateGraph: " + arguments);
        }

        String graphName = arguments.get(0);
        createGraph(graphName);
    }

    private void createGraph(String graphName) {
        MarvelPaths g1 = new MarvelPaths(graphName);
        graphs.put(graphName, g1);
        output.println("created graph "+ graphName);
    }

    private void addNode(List<String> arguments) {
        if(arguments.size() != 2) {
            throw new CommandException("Bad arguments to AddNode: " + arguments);
        }

        String graphName = arguments.get(0);
        String nodeName = arguments.get(1);

        addNode(graphName, nodeName);
    }

    private void addNode(String graphName, String nodeName) {
        //create new graph
        MarvelPaths g1 = graphs.get(graphName);
        //create new node
        Node<String,String> n = new Node<String,String>(nodeName);
        //add new node to the list
        g1.getGraphMap().addNode(n);
        output.println("added node " + n.getName() +" to " +graphName);
    }

    private void addEdge(List<String> arguments) {
        if(arguments.size() != 4) {
            throw new CommandException("Bad arguments to AddEdge: " + arguments);
        }

        String graphName = arguments.get(0);
        String parentName = arguments.get(1);
        String childName = arguments.get(2);
        String edgeLabel = arguments.get(3);

        addEdge(graphName, parentName, childName, edgeLabel);
    }

    private void addEdge(String graphName, String parentName, String childName,
                         String edgeLabel) {
        MarvelPaths g1 = graphs.get(graphName);
        Node<String,String> parent= g1.getGraphMap().getSpecificNode(parentName);
        Node<String,String> child = g1.getGraphMap().getSpecificNode(childName);
        //checks to make sure edge is not in the graph already
        if(g1.getGraphMap().getSpecificNode(parentName).getChildrenNodes().contains(g1.getGraphMap().getSpecificNode(childName))){
         if(g1.getGraphMap().getSpecificNode(parentName).GetNodeMap().get(g1.getGraphMap().getSpecificNode(childName)).contains(g1.getGraphMap().getSpecificEdge(edgeLabel)));
            output.println("Edge already in graph");
        }else {
            //adds edge to the graph
            g1.getGraphMap().addEdge(edgeLabel, parent, child);
            output.println("added edge " + edgeLabel + " from " + parentName + " to " + childName + " in " + graphName);
        }
    }

    private void listNodes(List<String> arguments) {
        if(arguments.size() != 1) {
            throw new CommandException("Bad arguments to ListNodes: " + arguments);
        }

        String graphName = arguments.get(0);
        listNodes(graphName);
    }

    private void listNodes(String graphName) {
        MarvelPaths g = graphs.get(graphName);
        List<Node<String,String>> nodeList = new ArrayList<>();
        //sorts list using implemented string comparator
        List<Node<String,String>> nodes = g.getGraphMap().getNodes();
        nodeList.addAll(nodes);
        nodeList.sort(new Comparator<Node<String,String>>() {
            @Override
            public int compare(Node<String,String>o1, Node<String,String> o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1.toString(), o2.toString());
            }
        });
        //if list is empty outputs empty string, else returns formatted output
        if(nodes.size()!=0) {
            output.println(graphName + " contains: " + nodeList.toString().replace("[", "").replace("]", "").replace(",", ""));
        }else{
            output.println(graphName + " contains:");
        }
    }

    private void listChildren(List<String> arguments) {
        if(arguments.size() != 2) {
            throw new CommandException("Bad arguments to ListChildren: " + arguments);
        }

        String graphName = arguments.get(0);
        String parentName = arguments.get(1);
        listChildren(graphName, parentName);
    }

    private void listChildren(String graphName, String parentName) {
        MarvelPaths g = graphs.get(graphName);
        List<String> newChildren = new ArrayList<>();
        //iterates through the list and returns node with name equal to parentName
        Node<String,String> tempNode = g.getGraphMap().getSpecificNode(parentName);
        HashMap<Node<String,String>,List<Edge<String,String>>> temp = tempNode.GetNodeMap();
        //gets the edges from each child node
        for(Map.Entry<Node<String,String>,List<Edge<String,String>>> tempMap: temp.entrySet()) {
            List<Edge<String,String>> edges = tempMap.getValue();
            //sets string value to child = edge for all edges
            for(int i = 0; i < edges.size();i++) {
                newChildren.add(tempMap.getKey() + "=" + edges.get(i));
            }
        }

        newChildren.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1, o2);
            }
        });
        output.println("the children of " + parentName +  " in " + graphName+ " are: " + newChildren.toString().replace("[","").replace("]","").replace(",",")").replace("{","").replace("}","").replace("=","(") + ")");
    }
    private void findPath(List<String> arguments) {
        if(arguments.size() != 3) {
            throw new CommandException("Bad arguments to FindPath: " + arguments);
        }
        String graphName = arguments.get(0);
        String parentNode = arguments.get(1);
        String childNode = arguments.get(2);
        findPath(graphName,parentNode,childNode);
    }

    private void findPath(String graphName, String parent, String child){
       MarvelPaths g = graphs.get(graphName);
       List<String> path = g.findPath(parent,child);
       // if there is not a node in the graph
       if(g.getGraphMap().getSpecificNode(parent) == null || g.getGraphMap().getSpecificNode(child)== null){
          //
           if(g.getGraphMap().getSpecificNode(parent)==null){
              output.println("unknown: " +parent);
          }
          //if the child node is null or not in the graph
          if(g.getGraphMap().getSpecificNode(child)==null){
              output.println("unknown: "+child);
          }
          return;
       }
            output.println("path from " + parent + " to " + child + ":");
           if(path.size() == 0) {
               output.println("no path found");
               return;
           }
            for (int i = 0; i < path.size() - 1; i++) {
                output.println(path.get(i) + " to " + path.get(i + 1) + " via " + g.getGraphMap().getSpecificNode(path.get(i)).getClosestEdge(g.getGraphMap().getSpecificNode(path.get(i + 1))));
            }


    }
    private void loadGraph(List<String> arguments) {
        if(arguments.size() != 2) {
            throw new CommandException("Bad arguments to LoadGraph " + arguments);
        }
        String name = arguments.get(0);
        String filename = arguments.get(1);
        loadGraph(name,filename);
    }

    private void loadGraph(String name, String filename){
        MarvelPaths g = new MarvelPaths(name, parser.parseData(filename));
        graphs.put(name,g);
        output.println("loaded graph "+name);
    }

    /**
     * This exception results when the input file cannot be parsed properly
     **/
    static class CommandException extends RuntimeException {

        public CommandException() {
            super();
        }

        public CommandException(String s) {
            super(s);
        }

        public static final long serialVersionUID = 3495;
    }
}
