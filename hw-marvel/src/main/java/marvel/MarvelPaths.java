package marvel;

import graph.GraphMap;
import graph.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.*;

/**
 *Collection of methods that construct and find paths through a graph and a main program to allow
 * client interaction
 */
public class MarvelPaths {

    //Representation invariant:
    //this != null && socialNetwork != null


    // did not add Abstract function since this is not an ADT

    private List<String> chars;
    private Set<String> books;
    private List<String> books2;
    private final GraphMap<String,String> socialNetwork;

    /**Constructs new Graph of parsed marvel data
     *
     * @param charsAndBooks A hashmap that parses the data from a database
     * @param name The name of the MarvelPaths object
     * @spec.effects This
     */
    //constructor if it is passing in a parsed file
    public MarvelPaths(String name,Map<String, List<String>> charsAndBooks){
        books = charsAndBooks.keySet();
        books2 = new ArrayList<>(books);
        socialNetwork = new GraphMap<String,String>(name);
        buildGraph(socialNetwork,charsAndBooks);
    }

    /**Constructor of simple Marvel path object with no file input
     *
     * @param name name of the MarvelPaths object
     * @spec.requires name != null
     */
    //Constructor for simple MarvelPath
    public MarvelPaths(String name){
        socialNetwork = new GraphMap<String,String>(name);
    }

    // private helper method to construct a graph from file input
    private void buildGraph(GraphMap<String,String> graph , Map<String,List<String>> books){

        //adds nodes to graph
        for(String b : books.keySet()){
            //get the list of characters associated with this specific comic book
            chars = books.get(b);
            //adds first node in the list
            socialNetwork.addNode(new Node<String,String>(chars.get(0)));
            for(int i = 0; i < chars.size(); i++) {
                for (int j = i+1; j < chars.size(); j++) {
                    //goes through character arraylist and adds edges bidirectionally between each associated character
                        socialNetwork.addNode(new Node<String,String>(chars.get(j)));
                        graph.addEdge(b, graph.getSpecificNode(chars.get(i)), graph.getSpecificNode(chars.get(j)));
                        graph.addEdge(b, graph.getSpecificNode(chars.get(j)), graph.getSpecificNode(chars.get(i)));

                }
            }
        }
    }
    //checks representation invariant for violations
    private void checkRep(){
        assert(socialNetwork!=null);
        assert(this!=null);
    }
    /**Finds shortest path between two nodes and returns list of nodes in path
     *
     * @param start Starting node in path
     * @param goal ending node in path
     *
     * @return Returns list of nodes in shortest path, if no path is found then empty list will be returned
     */
    public List<String> findPath(String start, String goal){
        checkRep();
        //initialize local variables and new hashmap to store paths
        HashMap<String, List<String>> path = new HashMap<>();
        Queue<Node<String,String>> queue = new LinkedList<Node<String,String>>();
        Node<String,String> goalNode = socialNetwork.getSpecificNode(goal);
        Node<String,String> n = socialNetwork.getSpecificNode(start);
        //if either node is not in the graph, empty list will be returned
        if(n == null || goalNode == null){
            return new ArrayList<>();
        }
        //initialization and start of the BFS
        List<Node<String,String>> children;
        //add current node to the queue
        queue.add(n);
        //add node to map and initialize arraylist to store path
        path.put(n.getName(), new ArrayList<>());
        path.get(n.getName()).add(n.getName());
        // while the working queue still has
        while(!queue.isEmpty()){
            //pop first node off of the queue
            n = queue.remove();
            //return if n is the destination
            if(n.equals(goalNode)){
               return path.get(goalNode.getName());
            }else{
                //gets set of children nodes from n
                children = n.getChildrenNodes();
                List<Node<String,String>> newChildren = new ArrayList<>();
                newChildren.addAll(children);
                //sorts child nodes to visit lexicographically first node
                newChildren.sort(new Comparator<Node<String,String>>() {
                    @Override
                    public int compare(Node<String,String> o1, Node<String,String> o2) {
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getName(), o2.getName());
                    }
                });
                //loops through the children of n and add to the queue. Also copies path from start to child into hashmap
                for(int i = 0; i < newChildren.size(); i++){
                      if(!path.containsKey(newChildren.get(i).getName())){
                        List<String> copy = new ArrayList<>();
                        //copies list of path so far and stores it into the hashmap.get(child)
                        copy.addAll(path.get(n.getName()));
                        copy.add(newChildren.get(i).getName());
                        path.put(newChildren.get(i).getName(),copy);
                        queue.add(newChildren.get(i));
                    }
                }
            }
        }
        checkRep();
        //will only reach this if no path was found
        return new ArrayList<>();

    }

    /**Returns the GraphMap object of MarvelPath
     *
     * @return Returns graph of MarvelPaths
     */
    public GraphMap<String,String> getGraphMap(){
        checkRep();
        return socialNetwork;
    }

    /**Accessor method to get all nodes in the MarvelPaths object
     *
     * @return Collection of all nodes in the MarvelPaths object
     */
    public List<Node<String,String>> getKey(){
        checkRep();
        return socialNetwork.getNodes();
    }

    /**Static method to run Marvel Program
     *
     * @param args command line argument(no command line arguments accepted until program is started)
     */
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        MarvelParser parser = new MarvelParser();
        MarvelPaths g1 = new MarvelPaths("g1", parser.parseData("marvel.csv"));
        String end = "";
        String start ="";
        //starts client program
            while (true) {
                System.out.println("Marvel database is loaded");
                System.out.println("Enter the starting node or type \"Exit\" to quit:");
                //scanner reads the input from the user
                start = scanner.nextLine();
                //makes sure user does not want to exit
                if (start.equalsIgnoreCase("Exit")) {
                    return;
                    //makes sure that user entered a valid character from the graph
                } else if (g1.socialNetwork.getSpecificNode(start) == null) {
                    System.out.println("\"" + start + "\" " + "is not a marvel character");
                } else {

                    System.out.println("Enter ending node or type \"Exit\" to quit:");
                    //captures input from the user for the end path
                    end = scanner.nextLine();
                    //checks if client wants to exit
                    if (end.equalsIgnoreCase("Exit")) {
                           return;
                    //verifies client entered a valid choice
                    } else if (g1.socialNetwork.getSpecificNode(end) == null) {
                            System.out.println("\"" + end + "\" " + "is not a marvel character");
                    //finds path from start to end
                    } else {
                           List<String> path = g1.findPath(start, end);
                           if (path.size() != 0) {
                               //formats the output to step1 to step2 via edge
                               for (int i = 0; i < path.size() - 1; i++) {
                                   System.out.println(path.get(i) + " to " + path.get(i + 1) + " via " + g1.socialNetwork.getSpecificNode(path.get(i)).getClosestEdge(g1.socialNetwork.getSpecificNode(path.get(i + 1))));
                                }
                            } else {
                                System.out.println("No Path Found!");
                            }
                        }
                    }
                System.out.println("");
                System.out.println("Ready to find the next path.");
            }
        }
}
