package graph;

/** <b>Edge</b> represents an <b>immutable</b> directional relationship between Nodes
 * in a graph. The edge will have a label, a parent node, and a child node. The
 * relationship between parent and child is that the edge will begin at the parent
 * and end at the child creating a path from parent to child.
 * @param <N> Type of Node objects
 * @param <E> Type of Edge objects
 */
public class Edge<N,E> {

//Abstraction function:
//Edge, e, represents a path between 2 vertices in a graph.
// each edge will be a path starting at the parent node and
//ending at the child node and will be named this.label
//
//representation invariant:
// this != null && this.parent != null && this.child != null
private Node<N,E> parent;
private Node<N,E> child;
private E label;
    /** Creates an edge between the 2 nodes a = parent, b = child
     *
     * @param label String to be name of the edge
     * @param a Node to be Parent node of the edge
     * @param b Node to be Child node of the edge
     * @throws IllegalArgumentException if label.length() == 0
     * @spec.requires Node A and B != null
     */
    public Edge(E label, Node<N,E> a, Node<N,E> b){
        this.label = label;
        this.parent = a;
        this.child=b;
        checkRep();
    }
    //checks that the rep invariant has not been violated
    private void checkRep(){
        assert(this != null);
        assert(this.parent != null);
        assert(this.child != null);
    }

    /**Returns the label of the edge
     *
     * @return Label of this.Edge
     * @spec.requires this != null
     */
    public E getLabel(){
        //accesses edge object and returns label of edge
        checkRep();
        return label;
    }
    /**Returns the Parent Node of this.Edge
     *
     * @return Parent Node of this.Edge
     * @spec.requires this != null
     */
    public Node<N,E> getParent(){
        checkRep();
        return parent;
    }

    /**Returns the Child Node of this.Edge
     *
     * @return Child Node of this.Edge
     * @spec.requires this != null
     */
    public Node<N,E> getChild(){
        checkRep();
        return child;
    }


    /**Returns String of Child node and edge label
     *
     * @return String representation of child nodes with edge label
     */
    @Override
    public String toString(){
        return this.getLabel().toString();
    }
    @Override
    public boolean equals(Object o1){
        if(!(o1 instanceof Edge<?,?>)){
            return false;
        }
        Edge<?,?> e = (Edge<?,?>) o1;
        return this.child.equals(e.child) && this.parent.equals(e.parent) && this.getLabel().equals(e.getLabel());
    }

    @Override
    public int hashCode(){
        return 31 * parent.hashCode() ^ child.hashCode();
    }

}
