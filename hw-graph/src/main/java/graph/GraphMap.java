package graph;

import java.security.Key;
import java.util.*;

/**<b>Graph</b> represents an <b>mutable</b> set of vertices(Nodes) and
 * with a set of ordered pairs of nodes represented by edges. This is a
 * directed graph, meaning that each order pair of nodes has a parent, child
 * relationship, where the edge starts at the parent and ends at the child
 * @param <N> Type of Node objects
 * @param <E> Type of Edge objects
 */

public class GraphMap<N,E> {
    //Abstraction Function:
//Graph, g, represents a graph of vertices and edges. Each path starts
//at a vertex and ends at another vertex. Each node of the graph is contained
//in this.nodes(each node is unique) and each edge is contained in this.edges
//If there are no nodes or edges then g represents an empty graph.

    //representation invariant:
//this != null &&
//this.edges != null &&
//this.nodes != null &&
//for all K in nodes <K,V> each this.nodes.get(K) is unique where unique refers to no duplicates in this.nodes
//for all K in edges <K,V> each this.edges.get(K) is unique where unique refers to no duplicates in this.edges
//for all K in this.nodes each node V != null
//for all K in this.edges each edge V != null
    private boolean DEBUG = false;
    private HashMap<N,Node<N,E>> nodes;
    private HashMap<E,Edge<N,E>> edges;
    //name of the graph
    private String name;

    /**
     * Constructs a new Graph
     * @param name Name of the Graph
     * @spec.effects Constructs a new Graph
     */
    public GraphMap(String name){
        this.name = name;
        nodes = new HashMap<>();
        edges = new HashMap<>();
        checkRep();
    }
    //checks that the rep invariant has not been violated
    private void checkRep(){
        assert(!this.equals(null));
        assert(!this.edges.equals(null));
        assert(!this.nodes.equals(null));
        if(DEBUG){
            List<N> tempNodeList = new ArrayList<>();
            tempNodeList.addAll(nodes.keySet());
            //tests that duplicate nodes are not in the list of nodes
            for(int i = 0; i < tempNodeList.size(); i++){
                    assert(!(nodes.get(tempNodeList.get(i)).equals(null)));
                }
            List<E> tempEdgeList = new ArrayList<>(edges.keySet());
            //tests that duplicate edges are not in the list of edges
            for(int j = 0; j < tempEdgeList.size(); j++){
                assert(!(edges.get(tempEdgeList.get(j)).equals(null)));
                }
            }
        }

    /**Returns a list of nodes that are in the graph
     *
     *
     * @return List of nodes in the graph
     * @spec.requires String Graph.nodes != null
     */
    public List<Node<N,E>> getNodes(){
        checkRep();
        List<Node<N,E>> lst = new ArrayList<>();
        for(Map.Entry<N,Node<N,E>> entry: this.nodes.entrySet())
            lst.add(entry.getValue());
        return new ArrayList<>(lst);
    }

    /**Returns a list of edges from the graph
     *
     * @return list of edges, will return empty list if there are no edges
     * @spec.requires graph != null
     */
      public List<Edge<N,E>> getEdges(){
            checkRep();
        //returns unmodifiable list to protect abstraction barrier
          List<Edge<N,E>> lst = new ArrayList<>();
          for(Map.Entry<E,Edge<N,E>> entry: this.edges.entrySet())
          lst.add(entry.getValue());
          return new ArrayList<>(lst);

    }

    /**Adds a new node to our graph
     *
     * @param node Node to be added to the graph
     * @spec.requires node != null
     * @spec.modifies Graph
     */
    public void addNode(Node<N,E> node){
        checkRep();
        //verifies list of nodes does not contain the same node
        if(!this.nodes.containsKey(node.getName())) {
            nodes.put(node.getName(), node);
        }
        checkRep();
    }
    /**Adds a path from this.Node to the specified Node
     * @param label the label of the new edge
     * @param n1 the node that will be the Parent Node of the Edge
     * @param n2 the Node that will be the Child Node on the Edge
     * @spec.modifies Graph
     * @spec.requires this != null
     */
    public void addEdge(E label, Node<N,E> n1, Node<N,E> n2) {
        checkRep();
        //iterates through list and makes sure similar edge is not being added

        if (edges.containsKey(label)){
            Edge<N,E> tempEdge = edges.get(label);
            if(tempEdge.equals(new Edge<N,E>(label,n1,n2))){
                return;
            }
        }
        //creates and adds new edge
        Edge<N,E> e = new Edge<N,E>(label,n1,n2);
        edges.put(label,e);
        n1.addChildtoList(n2,e);
        checkRep();
    }

    /**Returns specific node that has the name given to the method
     * @param label Name of the node you are searching for
     * @return Returns the node if found or null otherwise
     * @spec.requires Node in the Hashmap
     */
    public Node<N,E> getSpecificNode(N label) {
        if (this.nodes.containsKey(label)){
            return nodes.get(label);
        }else {
            return null;
        }
    }
    /**Returns specific node that has the name given to the method
     * @param label Name of the node you are searching for
     * @return Returns the node if found or null otherwise
     * @spec.requires Node in the Hashmap
     */
    public Edge<N,E> getSpecificEdge(String label) {
        if (this.nodes.containsKey(label)){
            return edges.get(label);
        }else {
            return null;
        }
    }

}
