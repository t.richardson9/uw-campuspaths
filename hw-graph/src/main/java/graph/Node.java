package graph;
import java.util.*;

/**<b>Node<N,/b> represents an <b>mutable</b> vertex of a Graph. A node is a point
 * on a graphing system that can be a landmark or other representation of a
 * point on a graph. A node consists of a name, parent and child nodes, and edges
 * between those nodes.
 *
 * @param <N> Type of Node objects
 * @param <E> Type of Edge objects
 */

public class Node<N,E> {
    //Abstraction Function
//Node,N, n, represents the node, or vertex, of a graph.
//Nodes contained in children:
// forall i in children:
// there is an edge <n,n.get(i)>

    //Representation Invariant:
//this.Node != null &&
//  this.children != null
// this.childrenNodes != null
// for i in childrenNodes


    //name of the node
    private N name;
    //list of children nodes
    private List<Node<N,E>> children;
    private HashMap<Node<N,E>,List<Edge<N,E>>> childrenNodes;
    /**
     * Constructs a new Node
N,     *
     *
     * @param name name to be set for the node
     * @spec.effects Constructs a new Node
N,     * @spec.requires String name.length() &gt; 0
     */
    public Node(N name){
        this.name = name;
        children = new ArrayList<>();
        childrenNodes = new HashMap<>();
        checkRep();
    }
    //checks that the rep invariant has not been violated
    private void checkRep(){
        assert(!this.equals(null));
        assert(!this.children.equals(null));
    }

    /**Adds child node to list of nodes
     * @param n node to add to the map
     * @param e Edge to add to the map
     */
    public void addChildtoList(Node<N,E> n, Edge<N,E> e) {
        if (!childrenNodes.containsKey(n)) {
            childrenNodes.put(n, new ArrayList<>());
            childrenNodes.get(n).add(e);
        } else {
            childrenNodes.get(n).add(e);
        }
    }
    /**Gets the name of this.Node N,and returns it as a String
     *
     * @spec.requires {this != null}
     * @return the name of the node as a String
     */
    public N getName(){
        checkRep();
       return name;
    }

    /**Returns a list of nodes that are children to this node
     *
     * @return list of nodes that are children to this node
     * @spec.requires childrenNodes != null
     */
    public List<Node<N,E>> getChildrenNodes(){
        Set<Node<N,E>> nodes = childrenNodes.keySet();
        List<Node<N,E>> nodeList = new ArrayList<>();
        nodeList.addAll(nodes);
        return new ArrayList<>(nodeList);
    }
    /**Returns the closest edge of the child node
     *
     * @param child the node that is requesting the edge from
     * @return Closest edge of child node
     */
    public Edge<N,E> getClosestEdge(Node<N,E> child){
        List<Edge<N,E>> ChildEdges = this.childrenNodes.get(child);
        ChildEdges.sort(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return String.CASE_INSENSITIVE_ORDER.compare(o1.getLabel().toString(),o2.getLabel().toString());
            }
        });

        return this.childrenNodes.get(child).get(0);
    }
    /**Returns Map of children nodes
     *
     * @return Returns Map of children nodes
     */
    public HashMap<Node<N,E>, List<Edge<N,E>>> GetNodeMap(){
        return childrenNodes;
    }
    @Override
    public String toString(){
        return this.getName().toString();
    }
    @Override
    public boolean equals(Object o1){
        if(!(o1 instanceof Node<?,?>)){
            return false;
        }
        Node<?,?> n = (Node<?,?>) o1;
        return this.getName().equals(n.getName());
    }

    @Override
    public int hashCode(){
    return 31*this.getName().hashCode();
    }
}
