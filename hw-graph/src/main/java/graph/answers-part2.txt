## CSE 331 hw-graph Part 2 Questions

NOTE: This file is used as part of hw-graph-part2, and is not due as part of the hw-graph-part1
submission. You may leave it blank for hw-graph-part1.

## Part 1
###########################################################################

A few examples of possible Graph representations:

    A: A collection of nodes and a collection of edges.
    B: An adjacency list, in which each node is associated with a list of its outgoing edges.
    C: An adjacency matrix, which explicitly represents, for every pair ⟨A,B⟩ of nodes, whether there 
       is a link from A to B, and how many.

1. In two or three sentences, explain an advantage and a disadvantage of each of the
representations listed above (for example, in terms of runtime complexity, space
complexity, or ease of implementation).

A:Advantage- You can access nodes and edges separately and can save computing time if all you need is edge info.
Ease of Implementation

Disadvantage-If you need to access nodes and edges together there will be added time to go through each list to
find what you need.

B:Advantage- Would require less space to store only the children of each node in the node class.

Disadvantage-Would be harder to implement and would need more traversal methods to sort through the
children of each node when searching for a path between multiple nodes.

C:Advantage- Would have all of the information needed for traversal of the list to find paths and
would be easy to implement.

Disadvantage- Would have to store a list of every pair and when scaling up to larger projects like Google Maps,
the collection of pairs would be very hard and time-consuming to sort through.


2. In two to three sentences, describe the representation you chose and explain why you
chose it. If you chose to use a different representation than one of the three we described
above, be sure to list its advantages and disadvantages, too.

I decided to use the collection of nodes and edges, because I feel that implementation will be much more
efficient and when we start using the graph, it would be much easier to access edges separately and traverse
edges, rather than Nodes and edges together.
Disadvantages may be that I do not know the whole algorithm and how we will search for a path, and maybe this is not
the best way to perform this task of the graph, but I am sure that I will be able to make it work.



## Part 4
###########################################################################

    (Please keep your answers brief and to-the-point.)

1. Describe any new tests you added and why you added them, or why you feel that your original
tests alone are sufficient.
I added some tests, for the script tests, to check that duplicate nodes can not be added to a graph verifying my rep invariant cannot be
violated. Added the test to create and test an empty graph which is a valid representation of a graph.



2. Did you make any changes to your specifications as you were implementing your Graph? (yes/no)
If yes, describe your changes and why you made them.
Yes,I changed the Graph class so that it no longer implements an iterable<node>, I did this because the
graph representation is a list of nodes and edges and already has a built-in iterator in each list. For this reason
I think it is redundant to add another iterator.

Also, I removed equals method for node because there can be nodes that have same name or paths, but are different nodes.
We just want to make sure that we are not putting the same object in the list twice which the object.equals method will cover.
For example, you do not want to put the same building into the map twice, but if there are similar buildings that are
actually different buildings, that would be ok.



