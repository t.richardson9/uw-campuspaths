package graph.junitTests;
import graph.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

/**
 * This class contains a set of test cases that can be used to test the implementation of the
 * Edge class.
 *
 * <p>
 */
public class EdgeTest {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(10); // 10 seconds max per method tested
    Node<String,String> n1 = new Node<>("n1");
    Node<String,String> n2 = new Node<>("n2");
    Node<String,String> n3 = new Node<>("n3");
    Node<String,String> n4 = new Node<>("n4");
    Edge<String,String> e23;
    Edge<String,String> e14;
    Edge<String,String> e32;
    Edge<String,String> e41;
    //setUp method sets up a graph with nodes and a few edges to test the methods in the edge class
    //it is important that the edges are in place as all the methods are accessor methods in edge class
    @Before
    public void setUp(){
    GraphMap<String,String> graph = new GraphMap<>("graphA");
    graph.addNode(n1);
    graph.addNode(n2);
    graph.addNode(n3);
    graph.addNode(n4);
    e23 = new Edge<String,String>("e23", n2, n3);
    e14 = new Edge<String,String>("e14", n1, n4);
    e32 = new Edge<String,String>("e32", n3, n2);
    e41 = new Edge<String,String>("e41", n4, n1);

    }

    ////////////////////////////////////////
    /////// Constructor test
    ///////////////////////////////////////
    @Test//tests that the constructor creates new edges with values
    public void testConstructor(){
        Edge<String,String> e12 = new Edge<>("e12", n1, n2);
        Edge<String,String> e12_2 = new Edge<>("e12", n1, n2);
        Edge<String,String> e13 = new Edge<>("e12", n1, n2);
    }
    ///////////////////////////////
    ////// getLabel Tests
    ///////////////////////////////

    @Test//tests the getLabel function to make sure that it returns the proper String value
    public void testGetLabel() {
        assertEquals(e23.getLabel(), "e23" );
        assertEquals(e14.getLabel(), "e14" );
        assertEquals(e32.getLabel(), "e32" );
        assertEquals(e41.getLabel(), "e41" );

    }

    ///////////////////////////////////
    //////  getChild Tests
    //////////////////////////////////
    @Test//tests to make sure accessor method to return child node is working properly
    public void testGetChild(){
        assertEquals(e23.getChild(), n3);
        assertEquals(e14.getChild(), n4);
        assertEquals(e32.getChild(), n2);
        assertEquals(e41.getChild(), n1);
        assertNotEquals(e41.getChild(), n2);
        assertNotEquals(e32.getChild(), n1);
        assertNotEquals(e14.getChild(), n2);
        assertNotEquals(e23.getChild(), n4);

    }
}
