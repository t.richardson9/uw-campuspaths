package graph.junitTests;
import graph.*;
import java.util.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

public class GraphTest {
   @Rule
    public Timeout globalTimeout = Timeout.seconds(10); // 10 seconds max per method tested
    Node<String,String> n1 = new Node<String, String>("n1");
    Node<String,String> n2 = new Node<String, String>("n2");
    Node<String,String> n3 = new Node<String,String>("n3");
    Node<String,String> n4 = new Node<String,String>("n4");
    GraphMap<String,String> graph = new GraphMap<>("graphA");

    //setUp method sets up a graph with nodes and a few edges to test the methods in the edge class
    //it is important that the edges are in place as all the methods are accessor methods in edge class
    @Before
    public void setUp() {

        graph.addNode(n1);
        graph.addNode(n2);
        graph.addNode(n3);
        graph.addNode(n4);

    }

    @Test
    public void testAddEdge(){
        graph.addEdge("e23", n2, n3);
        graph.addEdge("e14", n1, n4);
        graph.addEdge("e32", n3, n2);
        graph.addEdge("e41", n4, n1);
    }

    @Test
    public void testAddDuplicateEdges(){
        graph.addEdge("e23", n2, n3);
        graph.addEdge("e14", n1, n4);
        graph.addEdge("e32", n3, n2);
        graph.addEdge("e41", n4, n1);
        List<Edge<String,String>> edge1 = graph.getEdges();
        graph.addEdge("e23", n2, n3);
        graph.addEdge("e14", n1, n4);
        graph.addEdge("e32", n3, n2);
        graph.addEdge("e41", n4, n1);
        assertEquals(edge1, graph.getEdges());
        graph.addEdge("e23", n2, n3);
        graph.addEdge("e14", n1, n4);
        graph.addEdge("e32", n3, n2);
        graph.addEdge("e41", n4, n1);
        graph.addEdge("e41_2", n4, n1);
        assertNotEquals(edge1, graph.getEdges());
    }

    @Test
    public void testAddDuplicateNode(){
        List<Node<String,String>> nodes = new ArrayList<>();
        nodes.addAll(graph.getNodes());
        graph.addNode(n1);
        graph.addNode(n2);
        graph.addNode(n3);
        graph.addNode(n4);
        assertEquals(nodes, graph.getNodes());
        graph.addNode(new Node<String, String>("n8"));
        graph.addNode(new Node<String, String>("n7"));
        assertNotEquals(nodes,graph.getNodes());
        }

}
