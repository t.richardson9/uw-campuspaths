package graph.junitTests;
import graph.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
/**
 * This class contains a set of test cases that can be used to test the implementation of the
 * Node class.
 *
 * <p>
 */
public class NodeTest {

  /*  @Rule
    public Timeout globalTimeout = Timeout.seconds(10); // 10 seconds max per method tested
    //declaration of graphs, nodes, and list to be used in the tests
    GraphMap graphA = new GraphMap("graphA");
    GraphMap graphB = new GraphMap("graphB");
    List<Node> ChildrenN1 = new ArrayList<>();
    List<Node> ChildrenN2 = new ArrayList<>();
    List<Node> ChildrenN3 = new ArrayList<>();
    List<Node> ChildrenN4 = new ArrayList<>();
    List<Node> ChildrenN5 = new ArrayList<>();
    List<Node> ChildrenN6 = new ArrayList<>();
    List<Node> ChildrenN7 = new ArrayList<>();

    private Node n1;
    private Node n2;
    private Node n3;
    private Node n4;
    private Node n5;
    private Node n6;
    private Node n7;


    /////////////////////////////////////
    ////// Constructor test
    ////////////////////////////////////
    @Test
    public void testConstructor() {
      //test to make sure constructor is working properly
        n1 = new Node("n1");
        n2 = new Node("n2");
        n3 = new Node("n3");
        n4 = new Node("n4");
        n5 = new Node("n5");
        n6 = new Node("n6");
    }


    //setup 2 graphs that each contains 3 nodes and 3 edges to test the methods of the node class and
    //verify implementation is correct, more tests will be added later, but these tests will satisfy
    //the core requirements of a node
    @Before
    public void setUp() {
        //add nodes first
        n1 = new Node("n1");
        n2 = new Node("n2");
        n3 = new Node("n3");
        n4 = new Node("n4");
        n5 = new Node("n5");
        n6 = new Node("n6");
        graphA.addNode(n1);
        graphA.addNode(n2);
        graphA.addNode(n3);
        //add edges on the nodes
        graphA.addEdge("e12", n1, n2);
        graphA.addEdge("e13", n1, n3);
        graphA.addEdge("e32", n3, n2);
        graphB.addNode(n4);
        graphB.addNode(n5);
        graphB.addNode(n6);
        graphA.addEdge("e45", n4, n5);
        graphA.addEdge("e54", n5, n4);
        graphA.addEdge("e56", n5, n6);
        //add node to arraylist to compare with getChildren later
        ChildrenN1.add(n2);
        ChildrenN1.add(n3);
        ChildrenN3.add(n2);
        ChildrenN4.add(n5);
        ChildrenN5.add(n4);
        ChildrenN5.add(n6);



    }

    //goes through each list and verifies that all elements are in each list
    public boolean eq(List<Node<?>> a, List<Node<?>> b){
        if(a.size() != b.size()){
            return false;
        }
        for(int i = 0;i < a.size()-1; i++){
            boolean isTrue = a.get(i).getName().equals(b.get(i).getName());
            if(!isTrue) {
                return false;
            }
        }
        return true;
    }

    ////////////////////////////////////////////////
    ////////  Test getChildren()
    ///////////////////////////////////////////////
  //  @Test//tests accessor method to return expected list
  //  public void test_getChildren() {
   // assertTrue(eq(ChildrenN1, n1.getChildren()));
  //  assertTrue(eq(ChildrenN2, n2.getChildren()));
  //  assertFalse(eq(ChildrenN1, n2.getChildren()));
 //   assertFalse(eq(ChildrenN2, n1.getChildren()));
  //  }


    /////////////////////////////////////////////
    //////// getName tests
    ////////////////////////////////////////////

    @Test//test for the accessor method getName that returns the string value of the node name
    public void testStringName(){
        assertEquals(n1.getName(),"n1");
        assertEquals(n2.getName(),"n2");
        assertEquals(n4.getName(),"n4");
        assertEquals(n3.getName(),"n3");
        assertNotEquals(n1.getName(),"n2");
        assertNotEquals(n2.getName(),"n1");
        assertNotEquals(n4.getName(),"n3");
        assertNotEquals(n3.getName(),"n4");

    }
*/
}
