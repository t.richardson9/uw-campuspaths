/*
 * Copyright (C) 2022 Kevin Zatloukal and James Wilcox.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Autumn Quarter 2022 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

import React, {Component} from 'react';

interface EdgeListState{
    userInput:string
}

interface EdgeListProps {
    edgeList:edge[]
    onChange(edges: edge[]):void;  // called when a new edge list is ready
}
interface edge{
    key:number
    x1: number;
    y1: number;
    x2: number;
    y2: number;
    color: string;

}

/**
 * A text field that allows the user to enter the list of edges.
 * Also contains the buttons that the user will use to interact with the app.
 */
class EdgeList extends Component<EdgeListProps, EdgeListState> {

    constructor(props:any) {
        super(props);
        let val:string = ""
        this.state={
            userInput:val
        };
    }
    //handles click if the draw button is clicked
    handleClick(s:string, previousLst:edge[]): edge[] {
        let lst: edge[] = []
        if(s !== undefined) {
            //parsed string by line and stored in parsedByLine var
            let parsedByLine: string[] = s.split("\n")
            for (let i=0; i<parsedByLine.length;i++) {
                //parses each line into formatted edges, not validated until app validates
                let parsedByWord: string[] = parsedByLine[i].split(" ")
                let num1: number = parseInt(parsedByWord[0])
                let num2: number = parseInt(parsedByWord[1])
                let num3: number = parseInt(parsedByWord[2])
                let num4: number = parseInt(parsedByWord[3])
                let color1: string = parsedByWord[4]
                    //creates new edge with parsed info
                let e: edge = {key:i, x1: num1, y1: num2, x2: num3, y2: num4, color: color1};
                //adds new edge to edge list
                lst.push(e)
            }
            return lst
        }else{
            //if valid input is not received drawn lines remain on the map
            alert("Invalid input! Try changing the coordinates and trying again")
            return previousLst
        }
    }
    //called if the clear button is pressed and clears the textarea box and changes state to no edges
    handleOnClear():void{
        let newState = {
            userInput:""
        }
        this.setState(newState)
        this.props.onChange([]);
    }
    //sets new state to user input in the text area
    textValue(s:string):void{
        let newState ={
            userInput: s
        }
        this.setState(newState)
    }

    render() {
        return (
            <div id="edge-list">
                Edges <br/>
                <textarea id="myTextArea"
                    rows={5}
                    cols={30}
                    onChange={(event) => {
                        this.textValue(event.target.value)
                    }}
                    value={this.state.userInput}
                    placeholder={"Enter your Coordinates       format per line must be exactly \"X1 Y1 X2 Y2 Color\"\n per line"}>
                </textarea> <br/>
                <button onClick={() => {
                    this.props.onChange(this.handleClick(this.state.userInput,this.props.edgeList))}}>Draw</button>
                <button onClick={() => {
                    this.handleOnClear();}}>Clear</button>
            </div>
        );
    }
}
export default EdgeList;
