/*
 * Copyright (C) 2022 Kevin Zatloukal and James Wilcox.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Autumn Quarter 2022 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

import React, { Component } from "react";
import EdgeList from "./EdgeList";
import Map from "./Map";

// Allows us to write CSS styles inside App.css, any styles will apply to all components inside <App />
import "./App.css";

interface edge {
    key:number
    x1: number;
    y1: number;
    x2: number;
    y2: number;
    color: string;


}
interface AppState {
    lines: edge[]
}

class App extends Component<{}, AppState> { // <- {} means no props.

  constructor(props: any) {
    super(props);
      this.state = {
          lines: []
      };
  }

  render() {
      return (
      <div>
        <h1 id="app-title">Line Mapper!</h1>
        <div>
          <Map edgeList={this.state.lines}/>
        </div>
        <EdgeList
            //passes edgeList to Map
            edgeList={this.state.lines}
            //callback from EdgeList to return the active edges that user wants to draw currently
          onChange={(value) => {
              //if edges are valid edges and valid formatting
              if(this.validateEdge(value)) {
                  //changes state to new state with the current edges
                let newState = {
                  lines:value
                };
                this.setState(newState)
              }else{
                  //if edges are invalid user is alerted to try again
                  alert("Invalid input! Please try again.")
              }
          }}
        />
      </div>
    );
  }
  //checks to make sure that an edge is a valid edge where x and y values are between (0-4000)
   validateEdge(e:edge[]):boolean {
      //loops through each edge and validates each one, will return false if any formatting is incorrect on any line
      for (let i in e) {
           if (!(e[i].x1 >= 0 && e[i].x1 <= 4000) || !(e[i].y1 >= 0 && e[i].y1 <= 4000) || !(e[i].x2 >= 0 && e[i].x2 <= 4000) || !(e[i].y2 >= 0 && e[i].y2 <= 4000)) {
               return false;
           }
       }
       return true;
   }
}
export default App;
