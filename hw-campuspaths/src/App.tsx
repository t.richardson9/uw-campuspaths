/*
 * Copyright (C) 2022 Kevin Zatloukal and James Wilcox.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Autumn Quarter 2022 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

import React, {Component} from 'react';

// Allows us to write CSS styles inside App.css, any styles will apply to all components inside <App />
import "./App.css";
import Map from "./Map";
import Points from "./Points";
interface edge{
    key:number
    cost:number
    x1: number;
    y1: number;

    x2:number;
    y2:number;
}

interface AppState {
    lines:edge[]
    buildings:string[]

}

class App extends Component<{}, AppState> {
    constructor(props: any) {
        super(props);
        this.state = {
            lines: [],
            buildings: []
        };
    }

    //init function that captures the building names from the sever to populate dropdown in points class
    init = async() => {
        let buildings:string|undefined = await this.getBuildingNames()
        //sets building names to new state so that it can be passed to points class
        if((buildings !== undefined)){
            //parses the building names into a list of building names
            let buildingLst = buildings.split(",")
            let newState = {
                lines:this.state.lines,
                buildings:buildingLst
            }
            this.setState(newState)
        }else {
            return
        }
    }
    //async function to fetch building names to be used in itit function
    getBuildingNames = async ():Promise<string|undefined> => {
        try {
            //calls the getNames spark route to process to retrieve building names from server
            let responsePromise = fetch("http://localhost:4567/getNames");
            let response = await responsePromise;
            if (!response.ok) {
                alert("The status is wrong! Expected in response: 200, Was: " + response.status);
                return; // Don't keep trying to execute if the response is bad.
            }
            //captures a long string of the building names
            let textPromise = response.text();
            let text = (await textPromise) as string;
            return text
        } catch (e) {
            alert("There was an error contacting the server.");
            console.log(e);  // Logging the error can be nice for debugging.
        }
    };
    //findpath function will fetch the path data from one point to another
    findPath = async (start:string, end:string):Promise<string|undefined> =>{
        try {
            let findPathPromise = fetch("http://localhost:4567/findPath?start="+start+"&end="+end)
            let response = await findPathPromise;
        if (!response.ok) {
            alert("The status is wrong! Expected in response: 200, Was: " + response.status);
            return; // Don't keep trying to execute if the response is bad.
        }
        let textPromise = response.json();
        let pathData = await textPromise;
        let edgelist: edge[] = []
            //if the user tries to find a path to the same point
            if(pathData.path.length === 0){
            let e: edge = {
                key: 0,
                x1: pathData.start.x,
                y1: pathData.start.y,
                x2: pathData.start.x,
                y2: pathData.start.y,
                cost: 0
            }
            edgelist.push(e)
          //if user tries to get path from 2 different points
        }else {
                //parses data in array from path to edges to be drawn in map
            for (let i = 0; i < pathData.path.length; i++) {
                let e: edge = {
                    key: i,
                    x1: pathData.path[i].start.x,
                    y1: pathData.path[i].start.y,
                    x2: pathData.path[i].end.x,
                    y2: pathData.path[i].end.y,
                    cost: pathData.path[i].cost
                }
                edgelist.push(e)
            }
        }
            //sets new state to new edges to be passed to map and points class
            let newState = {
                lines: edgelist,
                buildings: this.state.buildings
            }
            this.setState(newState)

            return pathData.path
    } catch (e) {
        alert("Invalid start or end point. Try changing start or end point and try again");
        console.log(e);  // Logging the error can be nice for debugging.
    }
};


    render() {
        //calls init function to set up app for user
        this.init().then()
        return (
                <div>
                <h1 id="app-title">Campus Map!</h1>
                <div>
                    <Map
                        //passes edgelist props to map class to draw edges on map
                        edgeList={this.state.lines}
                    />
                </div>
                <Points
                    //passes props to points class
                    edges = {this.state.lines}
                    buildings ={this.state.buildings}
                     onChange={(value) => {
                         //as long as there is a valid start and end point we will call the findpath function
                         if(value.length !== 0) {
                             let start: string = value[0]
                             let end: string = value[1]
                             this.findPath(start, end).then()
                         }else {
                             let newState = {
                                 lines: [],
                                 buildings: this.state.buildings
                             }
                             this.setState(newState)
                         }
                    }}
                />
            </div>
        );
    }
}
export default App;
