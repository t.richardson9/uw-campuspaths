/*
 * Copyright (C) 2022 Kevin Zatloukal and James Wilcox.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Autumn Quarter 2022 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

import { LatLngExpression } from "leaflet";
import React, { Component } from "react";
import { MapContainer, TileLayer } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import MapLine from "./MapLine";
import { UW_LATITUDE_CENTER, UW_LONGITUDE_CENTER } from "./Constants";

// This defines the location of the map. These are the coordinates of the UW Seattle campus
const position: LatLngExpression = [UW_LATITUDE_CENTER, UW_LONGITUDE_CENTER];

// NOTE: This component is a suggestion for you to use, if you would like to. If
// you don't want to use this component, you're free to delete it or replace it
// with your hw-lines Map

interface edge{
    key:number
    cost:number
    x1: number;
    y1: number;
    x2:number;
    y2:number;
}

interface MapProps {
 edgeList: edge[]
}

interface MapState {}

class Map extends Component<MapProps, MapState> {

    render() {
        if (this.props.edgeList.length > 0) {
            return (
                <div id="map">
                    <MapContainer
                        center={position}
                        zoom={15}
                        scrollWheelZoom={false}
                    >
                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {//loops through each edge in the edgelist and draws each edge on the map
                        }
                        <ol>
                            {this.props.edgeList.map((e)=>(<MapLine key={e.key} color={"red"} x1={e.x1} y1={e.y1} x2={e.x2} y2={e.y2}/>))}
                       </ol>
                    </MapContainer>
                </div>
            );
            //will go into else statement only if there are no edges in edgeList prop
        } else {
            return (
                <div id="map">
                    <MapContainer
                        center={position}
                        zoom={15}
                        scrollWheelZoom={false}
                    >
                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                    </MapContainer>
                </div>
            );
        }
    }
                            }

export default Map;
