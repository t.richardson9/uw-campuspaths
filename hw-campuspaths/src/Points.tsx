/*
 * Copyright (C) 2022 Kevin Zatloukal and James Wilcox.  All rights reserved.  Permission is
 * hereby granted to students registered for University of Washington
 * CSE 331 for use solely during Autumn Quarter 2022 for purposes of
 * the course.  No other use, copying, distribution, or modification
 * is permitted without prior written consent. Copyrights for
 * third-party components of this work must be honored.  Instructors
 * interested in reusing these course materials should contact the
 * author.
 */

import React, {Component} from 'react';
import Dropdown, {Option} from 'react-dropdown';

interface PointState{
    labelValue:string,
    start:string
    end:string
}
interface edge{
    key:number
    x1: number;
    y1: number;

    x2:number;
    y2:number;
}

interface PointListProps {
    edges: edge[]
    buildings:string[]
    onChange(points: string[]):void;  // called when a new edge list is ready


}

/**
 * A text field that allows the user to enter the list of edges.
 * Also contains the buttons that the user will use to interact with the app.
 */
class Points extends Component<PointListProps, PointState> {

    constructor(props:any) {
        super(props);
        this.state={
            labelValue:"",
            start:"",
            end:""
        };
    }
    //parses building data to add to dropdown menus
    options(): Option[]{
    let str:string[] = this.props.buildings
    let newOptions:Option[] = []
        for (let i=0; i<str.length;i++) {
            //parses into Option object to be passed to dropdown options field
            let o:Option = {
                label: str[i].replace("\"", "").replace("[","").replace("]","").replace("\"",""),
                value: str[i].replace("\"", "").replace("[","").replace("]","").replace("\"","")
            }
            newOptions.push(o)
        }
        return newOptions
     }


    //handles click if the draw button is clicked
    handleClick(s:string,e:string): string[] {
        //removes the long name from the short name to pass short name to the app class for the findpath method
        if(s !== undefined || e !== undefined){
        let start:string[] = s.split("-")
        let newStart = start[0]
        let end:string[] = e.split("-")
        let newEnd = end[0]
        let points:string[] = []
        points.push(newStart)
        points.push(newEnd)
            return points
        }else{
            alert("Select a start and end point and try again")
            let prev:string[] = []
            prev.push(this.state.start)
            prev.push(this.state.end)
            return prev
        }
    }
    //called if the clear button is pressed and clears the dropdown boxes and changes state to no points on map
    handleOnClear():void{
        let newState = {
            labelValue:"",
            start:"",
            end:""
        }
        this.setState(newState)
        console.log(this.state)
        this.props.onChange([]);
    }
    render() {

        return (
            <div id="points">
                <button onClick={() => {
                    if(this.state.start !== '' && this.state.end !== '') {
                        let s: string[] = this.handleClick(this.state.start, this.state.end)
                        this.props.onChange(s)
                        let newState = {
                            labelValue: "Displaying path from " + this.state.start + " to " + this.state.end,
                            start: "",
                            end: ""
                        }
                        this.setState(newState)
                    }else{
                        alert("Please enter both a start and end point before finding a path!")
                    }
                    }}>Find Path</button>
                <button onClick={() => {
                    this.handleOnClear();}}>Reset</button>
                <div style={{marginTop:"10px",marginBottom:"20px",color:"blue"}}>
                <label style={{}}>
                    {this.state.labelValue}
                </label>
                </div>
                <div id="dropdown" style={{marginTop:"0px",marginBottom: "10px", marginLeft: "10px", marginRight:"10px"}}>
                    Start point:
                    <div style={{cursor: "pointer", marginTop:"0px",marginBottom: "10px", marginLeft: "0px",
                        marginRight:"auto",background: "url(http://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/br_down.png) no-repeat right #ddd",
                        border: "3px solid #ccc",borderBottom:"1px solid line", width:"400px",display:"block"}}>
                        {//dropdown for start point
                        }
                    <Dropdown
                     className="start-point"
                     value = {this.state.start}
                     options={this.options()}
                     onChange={(event)=>{
                             let newState = {
                                 start: event.value,
                                 end: this.state.end
                             }
                             this.setState(newState)
                             console.log(newState)
                         }
                     }
                     placeholder="Please select a starting location"/>
                    </div>
                    End point:
                    <div style={{cursor: "pointer",marginTop:"0px",marginBottom: "10px",
                        marginLeft: "0px", marginRight:"auto",border: "3px solid #ccc",
                        borderBottom:"1px solid line", width:"400px",
                        background:"url(http://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/br_down.png) no-repeat right #ddd"}} >
                        {//dropdown for endpoint
                        }
                    <Dropdown
                    className="end-point"
                    value = {this.state.end}
                    options={this.options()}
                    onChange={(event)=>{
                            let newState = {
                                start: this.state.start,
                                end: event.value
                            }
                            this.setState(newState)
                            console.log(newState)
                        }
                    }
                    placeholder=" Please select an ending location"/>
                    </div>
                </div>

            </div>
        );
    }
}
export default Points;
